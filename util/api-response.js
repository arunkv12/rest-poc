const Success = { code: 0, msg: "Success" };
const Error = { code: 500, msg: "Error" };
const NoData = { code: 204, msg: "No Content" };
const BadRequest = { code: 400, msg: "Bad Request" };

module.exports = { Success, Error, NoData, BadRequest };
