const moment = require("moment");

validateRecordPostReq = (req) => {
  let stDt = moment(req.body.startDate);
  let endt = moment(req.body.endDate);
  let mnCt = Number(req.body.minCount);
  let mxCt = Number(req.body.maxCount);

  if (
    moment(req.body.startDate, "YYYY-MM-DD", true).isValid() &&
    moment(req.body.endDate, "YYYY-MM-DD", true).isValid() &&
    endt.diff(stDt, "days") > 0 &&
    mxCt > mnCt
  ) {
    //format the req.body to as required
    let startDate = new Date(req.body.startDate);
    startDate.setUTCHours(0, 0, 0, 0);
    let endDate = new Date(req.body.endDate);
    endDate.setUTCHours(23, 59, 59, 999);
    req.body = {
      startDate: startDate,
      endDate: endDate,
      minCount: Number(req.body.minCount),
      maxCount: Number(req.body.maxCount),
    };
    return true;
  } else {
    return false;
  }
};

module.exports = { validateRecordPostReq };
