const express = require("express");
const router = express.Router();
const records = require("./records");

router.use("/records", records);

module.exports = router;
