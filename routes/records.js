const express = require("express");
const recordsRouter = express.Router();
const {
  Success,
  Error,
  NoData,
  BadRequest,
} = require("../util/api-response.js");
const { get } = require("../db/connection.js");
//const { Connection } = require("../db/test-conn.js");
const { validateRecordPostReq } = require("../util/req-validator.js");

recordsRouter.route("/").post(async function (req, res) {
  if (validateRecordPostReq(req)) {
    get()
      .collection("records")
      .aggregate([
        {
          $set: {
            totalCount: { $sum: "$counts" },
          },
        },
        {
          $match: {
            createdAt: {
              $gte: req.body.startDate,
              $lt: req.body.endDate,
            },
            totalCount: {
              $gte: req.body.minCount,
              $lt: req.body.maxCount,
            },
          },
        },
        {
          $project: {
            _id: 0,
            key: "$key",
            createdAt: "$createdAt",
            totalCount: "$totalCount",
          },
        },
      ])
      .toArray()
      .then((results) => {
        if (results.length > 0) {
          Success.records = results;
          res.send(Success);
        } else {
          res.send(NoData);
        }
      })
      .catch((error) => {
        console.error(error);
        res.send(Error);
      });
  } else {
    res.send(BadRequest);
  }
});

module.exports = recordsRouter;
