# README #

### What is this repository for? ###

* Getir - The Challenge API
* 1.0.0
* repository :- https://arunkv12@bitbucket.org/arunkv12/rest-poc.git

### How do I get set up? ###
Environment:-
* Required node version 10x or above

Project setup:-
* Clone project.
* Run npm install for installing dependencies.

Dev mode:- npm run dev

Production mode:- npm run start

Running jest tests:- npm run test

### Who do I talk to? ###

* Arun Varghese