const mongoClient = require("mongodb").MongoClient;
const mongoDbUrl = process.env.mongodb_uri;
let mongodb;
let mongoclientInst;

function connect(callback) {
  //mongoClient.connect();
  mongoClient.connect(mongoDbUrl, (err, client) => {
    if (err) throw err;
    mongodb = client.db(process.env.db);
    mongoclientInst = client;
    callback();
  });
}
function get() {
  return mongodb;
}

function close(callback) {
  mongoclientInst.close();
  callback();
}

module.exports = {
  connect,
  get,
  close,
};
