const app = require("./app");
const port = parseInt(process.env.port, 10) || 3000;
const connection = require("./db/connection");

connection.connect(() =>
  app.listen(port, () => {
    console.log(`getir-poc app listening at http://localhost:${port}`);
  })
);
