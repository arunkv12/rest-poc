const express = require("express");
const app = express();
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");
const swaggerUI = require("swagger-ui-express");
const docs = require("./docs");

//load env
const dotenv = require("dotenv");
dotenv.config();

//connection.connect();
//router
const routes = require("./routes");

app.use(express.json());

//logger
var accessLogStream = fs.createWriteStream(
  path.join(__dirname, "/logs/access.log"),
  {
    flags: "a",
  }
);
app.use(morgan("combined", { stream: accessLogStream }));

/* app.get("/test", (req, res) => {
  res.status(200).send("Hello World!");
}); */

app.use("/api", routes);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(docs));

module.exports = app;
