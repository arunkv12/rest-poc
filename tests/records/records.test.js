const app = require("../../app");
const request = require("supertest");
const connection = require("../../db/connection");
const reqPayload = require("./req-payload.json");

beforeAll((done) => {
  connection.connect(done);
});

afterAll((done) => {
  connection.close(done);
});

describe("POST /api/records", () => {
  test("POST request for getting records", () => {
    return request(app)
      .post("/api/records")
      .send(reqPayload)
      .then((response) => {
        //console.log(response);
        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty("records");
      });
  });
});

/* describe("Test Get", () => {
  test("GET method", (done) => {
    request(app)
      .get("/test")
      .then((response) => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });
}); */
