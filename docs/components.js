module.exports = {
  components: {
    schemas: {
      record: {
        type: "object",
        properties: {
          startDate: {
            type: "date",
            description:
              "startDate in “YYYY-MM-DD” format and it should be less than endDate)",
            example: "2015-01-01",
          },
          endDate: {
            type: "date",
            description:
              "endDate in “YYYY-MM-DD” formatand its greater than startDate)",
            example: "2016-12-31",
          },
          minCount: {
            type: "number",
            description:
              "minCount in number and it minCount be greater than zero and less than maxCount)", // desc
            example: 10,
          },
          maxCount: {
            type: "number",
            description:
              "maxCount in number and it should be greater than zero and greater than minCount)", // desc
            example: 100,
          },
        },
      },
    },
  },
};
