module.exports = {
  openapi: "3.0.3",
  info: {
    title: "Getir - The Challenge API",
    description: "Getir - The Challenge API Documentation by Arun Varghese",
    version: "1.0.0",
    contact: {
      name: "Arun Varghese",
      email: "arun.kunnuvilayil@gmail.com",
    },
  },
};
