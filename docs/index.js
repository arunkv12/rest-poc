const basicInfo = require("./basic-info");
const records = require("./records");
const components = require("./components");

module.exports = {
  ...basicInfo,
  ...records,
  ...components,
};
