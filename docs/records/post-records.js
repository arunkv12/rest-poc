module.exports = {
  // operation's method
  post: {
    tags: ["Records"], // operation's tag
    description:
      "API for filtering the records. There are four parameters in request payload." +
      " Filter conditions - 1. startDate” and “endDate” fields will contain the date in a “YYYY-MM-DD” format and data using “createdAt” column 2. “minCount” and “maxCount” are for filtering the data. Sum of the “count” array in the documents should be between “minCount” and “maxCount”.", // short desc
    operationId: "fetchrecord", // unique operation id
    parameters: [], // expected params
    requestBody: {
      // expected request body
      content: {
        // content-type
        "application/json": {
          schema: { $ref: "#/components/schemas/record" },
        },
      },
    },
    // expected responses
    responses: {
      // response code
      200: {
        description: "Success", // response desc
      },
      // response code
      500: {
        description: "Server error", // response desc
      },
    },
  },
};
