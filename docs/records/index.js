const records = require("./post-records");

module.exports = {
  paths: {
    "/api/records": {
      ...records,
    },
  },
};
